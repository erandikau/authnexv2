﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authnex_V2.Models.BL_Request
{
    public class Device_Obj
    {
    }
    public partial class Device_Post_Request
    {
        public int User_ID { get; set; }
        public int Company_ID { get; set; }
        public string Uuid { get; set; }   // Smart phone UUID or ARTM unique ID
        public int Device_Type { get; set; }   // 1 Smart Phone, 2 Feture Phone 3 ATM
        public string Device_Make { get; set; }
        public string Device_Model { get; set; }
        public string BioSignedKey { get; set; }
    }
    public partial class AddNewDeviceObj
    {
        public int Device_ID { get; set; }
        public int User_ID { get; set; }
        public System.DateTime Reg_date { get; set; }
        public bool Active { get; set; }
        public string Uuid { get; set; }
        public int Device_Type { get; set; }
        public string Device_Make { get; set; }
        public string Device_Model { get; set; }
        public string BioSignedKey { get; set; }
    }
    public partial class ReturnNewDeviceObj
    {
        public int Device_ID { get; set; }
        public int User_ID { get; set; }
        public System.DateTime Reg_date { get; set; }
        public bool Active { get; set; }
        public string Uuid { get; set; }
        public int Device_Type { get; set; }
        public string Device_Make { get; set; }
        public string Device_Model { get; set; }
        public string BioSignedKey { get; set; }
    }
}