﻿using Authnex_V2.Models.BL_Request;
using Authnex_V2.Models.Utils;
using System.Linq;
using System;
using System.Text;
using System.Security.Cryptography;
using Authnex_v2.Models.DB;

namespace Authnex_V2.Models.BL
{
    public class UserBusnbiessLogics
    {
        private readonly string RETURN_Genaral_ERROR = "Error 1000, backend server dose not accsepat your request. Please try again later ";
        private readonly string RETURN_Session_ERROR = "Error 1001, Invalid Session or Session expired ";
        private readonly string RETURN_Company_ERROR = "Error 1002, Invalid Company or User not Assigned to given Company ";
        private readonly string RETURN_RSA_ERROR = "Error 1003, Invalid entry for public or private key. Please re-validate your User and Company inputs ";
        private readonly string RETURN_User_ERROR = "Error 1004, Incorrect user details or user already exsist for the company given";
        public string AddNewUser(New_User_Creation_Request InputPara)
        {
            try
            {
                User newUser = new User
                {
                    UserName = InputPara.UserName,
                    MappedUserId = InputPara.MappedUserId,
                    AddedDate = DateTime.Now,
                    LastModifiedDate = DateTime.Now,
                    CompanyId = InputPara.CompanyId,
                    IpAddress = InputPara.IpAddress,
                    IsActive = true,
                    IsArchived = false,
                    IsFinger = InputPara.IsFinger,
                    IsPushToken = InputPara.IsPushToken,
                    MobileNumber = InputPara.MobileNumber,
                    ModifiedDate = DateTime.Now,
                    PushToken = InputPara.PushToken,
                    AuthnexPublicKey = InputPara.UserPublicKey,
                    BioMaticApprovalToken = InputPara.BioMaticApprovalToken
                };
                using (var context = new Era_AuthnexDBEntities())
                {
                    var UerResult = context.Users.FirstOrDefault(b => b.UserName == InputPara.MappedUserId && b.CompanyId == InputPara.CompanyId);
                    if (UerResult == null)
                    {
                        var CompanyResult = context.Companies.FirstOrDefault(b => b.Id == InputPara.CompanyId);
                        if (CompanyResult != null)
                        {
                            String PublicKeyForUSer = "";
                            String PrivateKeyForAuthnex = "";
                            try
                            {
                                RSA_Utills rSa = new RSA_Utills();
                                string[] keyPair = new string[2];
                                keyPair = rSa.GenarateRSAKeys();

                                PrivateKeyForAuthnex = keyPair[0];
                                PublicKeyForUSer = keyPair[1];
                            }
                            catch
                            {
                                return RETURN_RSA_ERROR;
                            }

                            newUser.AuthnexPublicKey = PublicKeyForUSer;
                            newUser.AuthnexPvtKey = PrivateKeyForAuthnex;

                            context.Users.Add(newUser);
                            context.SaveChanges();
                            return PublicKeyForUSer;
                        }
                        else
                        {
                            return RETURN_Company_ERROR;
                        }
                    }
                    else
                    {
                        return RETURN_User_ERROR;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return RETURN_Genaral_ERROR;
            }
        }



    }
}
