﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authnex_V2.Models.BL_Request
{
    public partial class QR_Auth_Post_request
    {
        //companyId, Qr_Secret, QR_RequestTime, QR_ClientSession, QR_Genaraterd_time
        public string QR_String { get; set; }
        public int UserID { get; set; }
        public string Uuid { get; set; }
        public string AuthnexPublicKey { get; set; }
        public string PushMessage { get; set; }
        public string PushMessageHeader { get; set; }

    }
}