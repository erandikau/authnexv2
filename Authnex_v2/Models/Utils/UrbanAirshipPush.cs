﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Authnex_V2.Models.BL_Request;

namespace Authnex_V2.Models.Utils
{
    public class UrbanAirshipPush
    {
        public static bool PushIos(QrAuthPushNotofocationIOS ios, int deviceType, string authKey)
        {
            var jsonStr = JsonConvert.SerializeObject(ios);

            using (HttpClient clt = new HttpClient())
            {
                clt.BaseAddress = new Uri("https://go.urbanairship.com/api/push");

                clt.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                clt.DefaultRequestHeaders.TryAddWithoutValidation("Accept", "application/vnd.urbanairship+json; version=3;");
                clt.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", authKey);

                var httpContent = new StringContent(jsonStr, Encoding.UTF8, "application/json");
                HttpResponseMessage response = clt.PostAsync("", httpContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync();
                    return true;
                }
            }
            return false;
        }
    }
}