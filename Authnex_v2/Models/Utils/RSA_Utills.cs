﻿using System;
using System.Security.Cryptography;
using System.Linq;
using System.Xml;
using System.Xml.Linq;


namespace Authnex_V2.Models.Utils
{
    public class RSA_Utills
    {
        public string[] GenarateRSAKeys()
        {
            RSACryptoServiceProvider RSA11 = new RSACryptoServiceProvider();
            var xDoc = XDocument.Parse(RSA11.ToXmlString(true));
            string PrivateStringModulus = xDoc.Descendants("Modulus").Single().ToString();
            string PrivateStringExponent = xDoc.Descendants("Exponent").Single().ToString();

            //var xDoc1 = XDocument.Parse(RSA11.ToXmlString(false));
            //string PublicStringModulus = xDoc.Descendants("Modulus").Single().ToString();
            //string PublicStringExponent = xDoc.Descendants("Exponent").Single().ToString();
            int x1 = "<Modulus>".Length;
            int x2 = "</Modulus>".Length;
            int y1 = "<Exponent>".Length;
            int y2 = "</Exponent>".Length;

            string[] keyPair = new string[2];
            keyPair[1] = PrivateStringModulus.Substring(y1, PrivateStringModulus.Length - (y1 + y2));
            keyPair[0] = PrivateStringExponent.Substring(x1, PrivateStringExponent.Length - (x1 + x2));

            //keyPair[0] = PrivateStringModulus + PrivateStringExponent;
            //keyPair[1] = PublicStringModulus + PublicStringExponent;
            return keyPair;
        }
        public byte[] Encryption(byte[] Data, RSAParameters RSAKey, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    RSA.ToXmlString(true);
                    RSA.ImportParameters(RSAKey); encryptedData = RSA.Encrypt(Data, DoOAEPPadding);
                }
                return encryptedData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public byte[] Decryption(byte[] Data, RSAParameters RSAKey, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    RSA.ImportParameters(RSAKey);
                    decryptedData = RSA.Decrypt(Data, DoOAEPPadding);
                }
                return decryptedData;
            }
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());
                return null;
            }
        }
    }
}