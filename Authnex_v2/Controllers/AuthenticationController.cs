﻿using System;
using System.Web.Http;
using Authnex_V2.Models.BL_Request;
using Authnex_V2.Models.BL;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Authnex_V2.Controllers
{
    public class AuthenticationController : ApiController
    {
        private readonly string RETURN_Message_ERROR = "Error 6001 : Incorrect input parameters or bad message format. Please try again"; //ERError
        private readonly string RETURN_Device_ERROR = "Error 6002 : Bad device ID or incorrect format"; //ERDevice
        private readonly string RETURN_QR_ERROR = "Error 6003 : Bad QR code or incorrect format"; //ERQR
        private readonly string RETURN_Session_ERROR = "Error 6004 : Bad Session ID or incorrect format"; //ERSession
        private readonly string RETURN_AuthFail_ERROR = "Error 6003 : Authantication fail or bad format"; //ERAuthFail
        private readonly string RETURN_InternalServerError_ERROR = "Error 6004 : Internal Server Error, Please try agin later";  //ERInternalServerError

        [HttpPost, Route("api/QrUserAuthentication")]
        public IHttpActionResult QrUserAuthentication([FromBody] QR_Auth_Post_request ImputPara)
        {
            Task<string> task = Task<string>.Factory.StartNew(() =>
            {
                AuthenticationBusniessLogics newQrAuth = new AuthenticationBusniessLogics();
                var Company_Return_String = newQrAuth.newQrAuth(ImputPara);
                return (Company_Return_String.ToString());
            });

            int i = task.Result.ToString().IndexOf("ERError");
            if (i <= 0)
            {
                return BadRequest(string.Format("\"Error\":\"{0}\"", RETURN_Message_ERROR));
            }
            else
            {
                i = task.Result.ToString().IndexOf("ERDevice");
                if (i <= 0)
                {
                    return BadRequest(string.Format("\"Error\":\"{0}\"", RETURN_Device_ERROR));
                }
                else
                {
                    i = task.Result.ToString().IndexOf("ERQR");
                    if (i <= 0)
                    {
                        return BadRequest(string.Format("\"Error\":\"{0}\"", RETURN_QR_ERROR));
                    }
                    else
                    {
                        i = task.Result.ToString().IndexOf("ERSession");
                        if (i <= 0)
                        {
                            return BadRequest(string.Format("\"Error\":\"{0}\"", RETURN_Session_ERROR));
                        }
                        else
                        {
                            i = task.Result.ToString().IndexOf("ERAuthFail");
                            if (i <= 0)
                            {
                                return BadRequest(string.Format("\"Error\":\"{0}\"", RETURN_AuthFail_ERROR));
                            }
                            else
                            {
                                i = task.Result.ToString().IndexOf("ERInternalServerError");
                                if (i <= 0)
                                {
                                    return BadRequest(string.Format("\"Error\":\"{0}\"", RETURN_InternalServerError_ERROR));
                                }
                                else
                                {
                                    return Ok(task.Result);
                                }
                            }
                        }
                    }
                }
            }
        }


    }
}