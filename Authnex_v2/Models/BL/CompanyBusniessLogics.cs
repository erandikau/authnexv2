﻿using System;
using Authnex_v2.Models.DB;
using System.Linq;
using Authnex_V2.Models.BL_Request;
using Newtonsoft.Json;

namespace Authnex_V2.Models.BL
{
    public class CompanyBusniessLogics
    {
        private readonly string RETURN_Message_ERROR = "Error";
        public string AddCompany(Company_Post_Request ImputPara)
        {
            DateTime theDate = DateTime.Now;
            DateTime yearInTheFuture = theDate.AddYears(5);

            Company CSO = new Company();
            CSO.AddedDate = DateTime.Now;
            CSO.ApprovalConfirmationEndpoint = ImputPara.EndpointUrl;
            CSO.AsyncAuthenticateEndpoint = ImputPara.EndpointUrl;
            CSO.ClientBaseUrl = ImputPara.ClientBaseUrl;
            CSO.CompanyName = ImputPara.CompanyName;
            CSO.EndpointUrl = ImputPara.EndpointUrl;
            CSO.ExpireDate = yearInTheFuture;
            CSO.IpAddress = ImputPara.IpAddress;
            CSO.IsActive = true;
            CSO.IsArchived = false;
            CSO.IsUserExistingEndpoint = ImputPara.EndpointUrl;
            CSO.JoinDate = ImputPara.JoinDate;
            CSO.ModifiedDate = DateTime.Now;
            CSO.UrbunAppKey = ImputPara.PushAppKey;
            CSO.UrbunMasterKey = ImputPara.PushMasterKey;
            CSO.UserDetailsEndpoint = ImputPara.EndpointUrl;
            CSO.QR_Secret = ImputPara.QR_Secret;

            using (var context = new Era_AuthnexDBEntities())
            {
                context.Companies.Add(CSO);
                context.SaveChanges();

                var AddCompanyResult = context.Companies.SingleOrDefault(b => b.CompanyName == ImputPara.CompanyName);
                if (AddCompanyResult != null)
                {
                    Company_Return_Request NC = new Company_Return_Request();
                    NC.Company_id = AddCompanyResult.Id;
                    NC.ClientBaseUrl = AddCompanyResult.ClientBaseUrl;
                    NC.CompanyName = AddCompanyResult.CompanyName;
                    NC.EndpointUrl = AddCompanyResult.EndpointUrl;
                    NC.IpAddress = AddCompanyResult.IpAddress;
                    NC.JoinDate = AddCompanyResult.JoinDate;
                    NC.PushAppKey = AddCompanyResult.UrbunAppKey;
                    NC.PushMasterKey = AddCompanyResult.UrbunMasterKey;
                    NC.QR_Secret = AddCompanyResult.QR_Secret;
                    return JsonConvert.SerializeObject(NC).ToString();
                }
                else
                {
                    return RETURN_Message_ERROR;
                }
            }
        }
    }
}