﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authnex_V2.Models.BL_Request
{
    public partial class New_User_Creation_Request
    {
        public string UserName { get; set; }
        public string MappedUserId { get; set; }
        public int CompanyId { get; set; }
        public string IpAddress { get; set; }
        public bool IsFinger { get; set; }
        public bool IsPushToken { get; set; }
        public string MobileNumber { get; set; }
        public string PushToken { get; set; }
        public string BioMaticApprovalToken { get; set; }
        public string UserPublicKey { get; set; }
    }
}