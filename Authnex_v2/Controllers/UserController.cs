﻿using System.Web.Http;
using Authnex_V2.Models.BL;
using Authnex_V2.Models.BL_Request;

namespace Authnex_V2.Controllers
{
    public class UserController : ApiController
    {
        [HttpPost, Route("api/NewUserRegistrationRequest")]
        public IHttpActionResult NewUserRegistrationRequest([FromBody] New_User_Creation_Request InputPara)
        {
            UserBusnbiessLogics UBL = new UserBusnbiessLogics();
            string User_Return_String = UBL.AddNewUser(InputPara);
            int i = User_Return_String.IndexOf("Error");
            if (i <= 0)
            {
                return Ok(string.Format("\"Authnex_User_Public_Key\":\"{0}\"", User_Return_String));
            }
            else
            {
                return BadRequest(string.Format("\"Error\":\"{0}\"", User_Return_String));
            }
        }
    }
}
