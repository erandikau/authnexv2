﻿using System.Web.Http;
using Authnex_V2.Models.BL_Request;
using Authnex_V2.Models.BL;

namespace Authnex_V2.Controllers
{
    public class DeviceController : ApiController
    {
        private readonly string Err_BAD_REQUEST = "Error 3001 : Incorrect input parameters or bad message format. Please try again";

        [HttpPost, Route("api/AddNewDevice")]
        public IHttpActionResult AddNewDevice([FromBody] Device_Post_Request ImputPara)
        {
            DeviceBusniessLogics DBL = new DeviceBusniessLogics();
            string Dev_Return_String = DBL.AddNewDevice(ImputPara);
            int i = Dev_Return_String.IndexOf("Error");
            if (i >= 0)
            {
                return BadRequest(string.Format("\"Error\":\"{0}\"", Err_BAD_REQUEST));
            }
            else
            {
                return Ok(string.Format("\"Device_Data\":\"{0}\"", Dev_Return_String));
            }
        }
    }
}