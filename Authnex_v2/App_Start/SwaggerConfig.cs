using System.Web.Http;
using WebActivatorEx;
using Authnex_V2;
using Swashbuckle.Application;

[assembly: PreApplicationStartMethod(typeof(SwaggerConfig), "Register")]

namespace Authnex_V2
{
    public class SwaggerConfig
    {
        public static void Register()
        {
            var thisAssembly = typeof(SwaggerConfig).Assembly;

            GlobalConfiguration.Configuration
              .EnableSwagger(c => c.SingleApiVersion("v1", "A title for your API"))
              .EnableSwaggerUi();
        }
    }
}
