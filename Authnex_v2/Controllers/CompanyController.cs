﻿using System.Web.Http;
using Authnex_V2.Models.BL_Request;
using Authnex_V2.Models.BL;

namespace Authnex_V2.Controllers
{
    public class CompanyController : ApiController
    {
        private readonly string Err_BAD_REQUEST = "Error 5001 : Incorrect input parameters or bad message format. Please try again";

        [HttpPost, Route("api/AddNewCompany")]
        public IHttpActionResult AddNewCompany([FromBody] Company_Post_Request ImputPara)
        {
            CompanyBusniessLogics newCompany = new CompanyBusniessLogics();
            string Company_Return_String = newCompany.AddCompany(ImputPara);
            int i = Company_Return_String.IndexOf("Error");
            if (i <= 0)
            {
                return Ok(string.Format("\"Company_Data\":\"{0}\"", Company_Return_String));
            }
            else
            {
                return BadRequest(string.Format("\"Error\":\"{0}\"", Err_BAD_REQUEST));
            }
        }
    }
}