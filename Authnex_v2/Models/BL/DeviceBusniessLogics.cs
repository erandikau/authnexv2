﻿using Authnex_v2.Models.DB;
using Authnex_V2.Models.BL_Request;
using Newtonsoft.Json;
using System;
using System.Linq;


namespace Authnex_V2.Models.BL
{
    public class DeviceBusniessLogics
    {
        private readonly string RETURN_Message_ERROR = "Error";
        public string AddNewDevice(Device_Post_Request ImputPara)
        {
            int USerID = ImputPara.User_ID;
            int Company_ID = ImputPara.Company_ID;
            string Uuid = ImputPara.Uuid;
            int Device_Type = ImputPara.Device_Type;
            string Device_Make = ImputPara.Device_Make;
            string Device_Model = ImputPara.Device_Model;
            string BioSignedKey = ImputPara.BioSignedKey;

            using (var context = new Era_AuthnexDBEntities())
            {
                var UserResult = context.Users.SingleOrDefault(b => b.Id == USerID);
                if (UserResult != null)
                {
                    var CompanyResult = context.Companies.SingleOrDefault(b => b.Id == Company_ID);
                    if (CompanyResult != null)
                    {
                        var DeviceResult = context.Devices.SingleOrDefault(b => b.User_ID == USerID && b.Uuid == Uuid);
                        if (DeviceResult == null)
                        {
                            Device Dev = new Device
                            {
                                User_ID = USerID,
                                Company_ID = Company_ID,
                                Reg_date = DateTime.Now,
                                Active = true,
                                Uuid = Uuid,
                                Device_Type = Device_Type,
                                Device_Make = Device_Make,
                                Device_Model = Device_Model,
                                BioSignedKey = BioSignedKey
                            };
                            context.Devices.Add(Dev);
                            context.SaveChanges();

                            var AddDeviceResult = context.Devices.SingleOrDefault(b => b.User_ID == USerID && b.Uuid == Uuid);
                            if (AddDeviceResult != null)
                            {
                                ReturnNewDeviceObj RDO = new ReturnNewDeviceObj();
                                RDO.Device_ID = AddDeviceResult.Device_ID;
                                RDO.User_ID = AddDeviceResult.User_ID;
                                RDO.Reg_date = AddDeviceResult.Reg_date;
                                RDO.Active = AddDeviceResult.Active;
                                RDO.Uuid = AddDeviceResult.Uuid;
                                RDO.Device_Type = AddDeviceResult.Device_Type;
                                RDO.Device_Make = AddDeviceResult.Device_Make;
                                RDO.Device_Model = AddDeviceResult.Device_Model;
                                RDO.BioSignedKey = AddDeviceResult.BioSignedKey;

                                return JsonConvert.SerializeObject(RDO).ToString();
                            }
                            else
                            {
                                return RETURN_Message_ERROR;
                            }
                        }
                        else
                        {
                            return RETURN_Message_ERROR;
                        }
                    }
                    else
                    {
                        return RETURN_Message_ERROR;
                    }
                }
                else
                {
                    return RETURN_Message_ERROR;
                }
            }
        }
    }
}