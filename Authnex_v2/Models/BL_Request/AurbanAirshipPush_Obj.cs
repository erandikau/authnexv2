﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authnex_V2.Models.BL_Request
{
    public partial class QrAuthPushNotofocationIOS
    {
        public AudienceIOS audience { get; set; }
        public NotificationIOS notification { get; set; }
        public string[] device_types { get; set; }
    }
    public partial class AudienceIOS
    {
        public string Ios_channel { get; set; }
        //public string android_channel { get; set; }
    }
    public partial class NotificationIOS
    {
        public string alert { get; set; }
    }
}