﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Linq;
using Authnex_V2.Models.BL_Request;
using ZXing.QrCode;
using Authnex_v2.Models.DB;

namespace Authnex_V2.Models.BL
{
    public class QrBusniessLogics
    {
        private readonly string RETURN_ERROR = "Error ";
        private readonly string RETURN_Session_ERROR = "Error 2001 : Invalid Session or Session expired ";

        public string BL_DeviceQrRequest(QR_Post_Request ImputPara)
        {
            string QR_String = "";
            try
            {
                Random random = new Random();
                int x1 = random.Next(100000000, 299999999);
                int x2 = random.Next(200000000, 399999999);
                int x3 = random.Next(300000000, 499999999);
                int x4 = random.Next(400000000, 599999999);

                String timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                string _SessionKey = x1.ToString() +
                    x2.ToString() +
                    x3.ToString() +
                    timeStamp +
                    x4.ToString();

                int USerID = ImputPara.userID;
                int companyId = ImputPara.QR_CompanyID;
                int DeviceID = ImputPara.QR_DeviceID;
                string UUId = ImputPara.Uuid;
                string Qr_Secret = ImputPara.QR_Secret;
                DateTime QR_RequestTime = ImputPara.UserRequestedTime;
                string QR_ClientSession = _SessionKey;
                DateTime QR_Genaraterd_time = DateTime.Now;

                var QrcodeContent = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}", companyId, USerID, DeviceID, Qr_Secret, QR_RequestTime, QR_ClientSession, QR_Genaraterd_time);
                var qrCodeWriter = new ZXing.BarcodeWriterPixelData
                {
                    Format = ZXing.BarcodeFormat.QR_CODE,
                    Options = new QrCodeEncodingOptions
                    {
                        Height = 250,  // QR Height
                        Width = 250,   // QR Width
                        Margin = 0
                    }
                };

                var pixelData = qrCodeWriter.Write(QrcodeContent);

                using (var bitmap = new System.Drawing.Bitmap(pixelData.Width, pixelData.Height, System.Drawing.Imaging.PixelFormat.Format32bppRgb))
                {
                    using (var ms = new MemoryStream())
                    {
                        var bitmapData = bitmap.LockBits(new System.Drawing.Rectangle(0, 0, pixelData.Width, pixelData.Height), System.Drawing.Imaging.ImageLockMode.WriteOnly, System.Drawing.Imaging.PixelFormat.Format32bppRgb);
                        try
                        {
                            // we assume that the row stride of the bitmap is aligned to 4 byte multiplied by the width of the image   
                            System.Runtime.InteropServices.Marshal.Copy(pixelData.Pixels, 0, bitmapData.Scan0, pixelData.Pixels.Length);
                        }
                        finally
                        {
                            bitmap.UnlockBits(bitmapData);
                        }
                        bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Png);
                        QR_String = Convert.ToBase64String(ms.ToArray());
                    }
                }
                QR newQR = new QR
                {
                    QR_CompanyID = companyId,
                    QR_UserID = USerID,
                    QR_DeviceID = DeviceID,
                    UserRequestedTime = QR_RequestTime,
                    QR_Genarated_Time = QR_Genaraterd_time,
                    SessionID = Convert.ToInt16("-999"),  // Not used in current implamantation.
                    QRSession = QR_ClientSession,
                    QR_Secret = Qr_Secret,
                    QR_Status = 0,  // 0 = New QR   1 = Read QR  2 = Expired QR
                };
                using (var context = new Era_AuthnexDBEntities())
                {
                    context.QRs.Add(newQR);
                    context.SaveChanges();
                }
                return QR_String;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return RETURN_ERROR;
            }
        }

        public string BL_QR_Status(QR_Post_Stats ImputPara)
        {
            int t_QR_ID = ImputPara.QR_ID;
            int t_Company_ID = ImputPara.QR_CompanyID;

            int USerID = ImputPara.userID;
            string QR_Secret = ImputPara.Uuid;
            SessionBusniessLogics SESSION = new SessionBusniessLogics();
            if (SESSION.validateSession(USerID, QR_Secret, "BL_QR_Status"))
            {
                using (var context = new Era_AuthnexDBEntities())
                {
                    var result = context.QRs.SingleOrDefault(b => b.QR_ID == t_QR_ID && b.QR_CompanyID == t_Company_ID);
                    if (result != null)
                    {
                        return JsonConvert.SerializeObject(result);
                    }
                    else
                    {
                        return RETURN_ERROR;
                    }
                }
            }
            else
            {
                return RETURN_Session_ERROR;
            }
        }
    }
}
