﻿using System;
using Authnex_v2.Models.DB;
using System.Linq;
using Authnex_V2.Models.BL_Request;
using Authnex_V2.Models.Utils;
using Newtonsoft.Json;

namespace Authnex_V2.Models.BL
{
    public class AuthenticationBusniessLogics
    {
        private readonly string UrbanAirshipKey = "bEtVc2ZNekNUT0czMFkyVzhtZkNpQTpmLTFGM2NKNVFzeXNRNHU2WC16bzF3";

        private readonly string RETURN_Device_ERROR = "ERDevice";
        private readonly string RETURN_QR_ERROR = "ERQR";
        private readonly string RETURN_Message_ERROR = "ERError";
        private readonly string RETURN_Session_ERROR = "ERSession";
        private readonly string RETURN_AuthFail_ERROR = "ERAuthFail";
        private readonly string RETURN_InternalServerError_ERROR = "ERInternalServerError";
        public string newQrAuth(QR_Auth_Post_request ImputPara)
        {

            string _QR_Code = ImputPara.QR_String;
            //_QR_Code = "1|TTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTTT|7/17/2019 12:08:38 PM|232687201366739448449473418201907171805254091410230751|7/17/2019 6:05:30 PM";
            char[] splitchar = { '|' };
            string[] QR= _QR_Code.Split(splitchar);

            int QRcompanyId = Convert.ToInt16(QR[0]);
            int QRUserID = Convert.ToInt16(QR[1]);
            int QR_Device_ID = Convert.ToInt16(QR[2]);
            string _Qr_Secret = QR[3];
            DateTime _QR_RequestTime = Convert.ToDateTime(QR[4]);
            string _QR_ClientSession = QR[5];
            DateTime _QR_Genaraterd_time = Convert.ToDateTime(QR[6]);

            int QR_Scaned_CompamyID;
            int QR_Scaned_UserID = ImputPara.UserID;
            string QR_Scaned_Uuid = ImputPara.Uuid;
            string _AuthnexPublicKey = ImputPara.AuthnexPublicKey;
            string _authMessage = ImputPara.PushMessage;
            string _messageHeader = ImputPara.PushMessageHeader;
            int _DeviceID;
            using (var context = new Era_AuthnexDBEntities())
            {
                var QrResult = context.QRs.SingleOrDefault(b => b.QR_CompanyID == QRcompanyId && b.QR_UserID == QRUserID && b.QRSession == _QR_ClientSession);
                if (QrResult != null)
                {
                    if (QrResult.QR_Status == 0)
                    {
                        QrResult.QR_Status = 1; // Mark as read QR
                        context.SaveChanges();

                        if (QrResult.QR_Secret == _Qr_Secret)
                        {
                            var DeviceResult = context.Devices.SingleOrDefault(b =>b.Device_ID == QR_Device_ID && b.  User_ID == QRUserID && b.Company_ID == QRcompanyId);
                            if (DeviceResult != null && DeviceResult.Device_ID == QR_Device_ID)
                            {
                                _DeviceID = QR_Device_ID;

                                var UserResult = context.Users.SingleOrDefault(b => b.Id == QR_Scaned_UserID);
                                if (UserResult.AuthnexPublicKey == _AuthnexPublicKey)
                                {
                                    QR_Scaned_CompamyID = UserResult.CompanyId;

                                    NotificationIOS notification = new NotificationIOS { alert = _messageHeader };
                                    AudienceIOS audience = new AudienceIOS
                                    {
                                        Ios_channel = UserResult.PushToken.Trim()
                                    };
                                    string[] DeviceType = { "ios" };
                                    QrAuthPushNotofocationIOS pushPayloadIos = new QrAuthPushNotofocationIOS
                                    {
                                        audience = audience,
                                        device_types = DeviceType,
                                        notification = notification
                                    };

                                    if (UrbanAirshipPush.PushIos(pushPayloadIos, 1, UrbanAirshipKey))
                                    {
                                        Random random = new Random();
                                        int x1 = random.Next(100000000, 299999999);
                                        int x2 = random.Next(200000000, 399999999);
                                        int x3 = random.Next(300000000, 499999999);
                                        int x4 = random.Next(400000000, 599999999);
                                        int x5 = random.Next(500000000, 699999999);
                                        int x6 = random.Next(600000000, 799999999);

                                        String timeStamp = DateTime.Now.ToString("yyyyMMddHHmmssffff");
                                        string _SessionKey = x1.ToString() +
                                            x2.ToString() +
                                            x3.ToString() +
                                            timeStamp +
                                            x4.ToString() +
                                            x5.ToString() +
                                            x6.ToString();

                                        Session s = new Session();
                                        s.UserID = QR_Scaned_UserID;
                                        s.CompanyID = QR_Scaned_CompamyID;
                                        s.DeviceID = _DeviceID;
                                        s.SessionDate = DateTime.Now;
                                        s.SessionStatus = 1;  //0 inactive 1 active
                                        s.SessionKey = _SessionKey;
                                        context.SaveChanges();

                                        return JsonConvert.SerializeObject(s).ToString();
                                    }
                                    else
                                    {
                                        return RETURN_InternalServerError_ERROR;
                                    }
                                }
                                else //2
                                {
                                    return RETURN_AuthFail_ERROR;
                                }
                            }
                            else
                            {
                                return RETURN_Device_ERROR;
                            }
                        }
                        else
                        {
                            return RETURN_Session_ERROR;
                        }
                    }
                    else
                    {
                        return RETURN_QR_ERROR;
                    }
                }
                else
                {
                    return RETURN_Message_ERROR;
                }
            }
        }
    }
}