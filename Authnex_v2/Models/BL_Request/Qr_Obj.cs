﻿using System;

namespace Authnex_V2.Models.BL_Request
{
    public class Qr_Obj
    {

    }
    public partial class QR_Post_Stats
    {
        public int userID { get; set; }
        public string Uuid { get; set; }
        public int QR_ID { get; set; }
        public int QR_CompanyID { get; set; }
    }
    public partial class QR_Post_Request
    {
        public int userID { get; set; }
        public int QR_CompanyID { get; set; }
        public int QR_DeviceID { get; set; }
        public string Uuid { get; set; }
        public DateTime UserRequestedTime { get; set; }
        public string QR_Secret { get; set; }
    }
}