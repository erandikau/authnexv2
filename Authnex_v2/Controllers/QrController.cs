﻿using Authnex_V2.Models.BL_Request;
using Authnex_V2.Models.BL;
using System.Web.Http;
using Authnex_V2.Controllers;
namespace Authnex_V2.Controllers
{
    public class QRController : ApiController
    {
        private readonly string Err_BAD_REQUEST = "Error 2001 : Incorrect input parameters or bad message format. Please try again";
        private readonly string Err_INVALID_SESSION = "Error 2002 : Invalid or expired session. Please validate your user ID or active UDID";

        [HttpPost, Route("api/DeviceQrRequest")]
        public IHttpActionResult DeviceQrRequest([FromBody] QR_Post_Request ImputPara)
        {
            QrBusniessLogics Genarate_QR = new QrBusniessLogics();
            string QR_Return_String = Genarate_QR.BL_DeviceQrRequest(ImputPara);
            int i = QR_Return_String.IndexOf("Error");
            if (i <= 0)
            {
                return Ok(string.Format("\"QR_Code\":\"{0}\"", QR_Return_String));
            }
            else
            {
                if (QR_Return_String.IndexOf("Error 2001") <= 0)
                {
                    return BadRequest(string.Format("\"Error\":\"{0}\"", Err_INVALID_SESSION));
                }
                else
                {
                    return BadRequest(string.Format("\"Error\":\"{0}\"", Err_BAD_REQUEST));
                }
            }
        }
        [HttpPost, Route("api/QrStatus")]
        public IHttpActionResult QrStatus([FromBody] QR_Post_Stats ImputPara)
        {
            QrBusniessLogics Genarate_QR = new QrBusniessLogics();
            string QR_Return_String = Genarate_QR.BL_QR_Status(ImputPara);
            int i = QR_Return_String.IndexOf("Error");
            if (i <= 0)
            {
                return BadRequest(string.Format("\"Error\":\"{0}\"", Err_BAD_REQUEST));
            }
            else
            {
                if (QR_Return_String.IndexOf("Error 2001") <= 0)
                {
                    return BadRequest(string.Format("\"Error\":\"{0}\"", Err_INVALID_SESSION));
                }
                else
                {
                    return Ok(string.Format("\"QR_Code\":\"{0}\"", QR_Return_String));
                }
            }
        }
    }
}