﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Authnex_V2.Models.BL_Request
{
    public class Company_Obj
    {
    }
    public partial class Company_Return_Request
    {
        public int Company_id { get; set; }
        public string ClientBaseUrl { get; set; }
        public string CompanyName { get; set; }
        public string EndpointUrl { get; set; }
        public string IpAddress { get; set; }
        public Nullable<System.DateTime> JoinDate { get; set; }
        public string PushAppKey { get; set; }
        public string PushMasterKey { get; set; }
        public string QR_Secret { get; set; }
    }
    public partial class Company_Post_Request
    {
        public string ClientBaseUrl { get; set; }
        public string CompanyName { get; set; }
        public string EndpointUrl { get; set; }
        public string IpAddress { get; set; }
        public Nullable<System.DateTime> JoinDate { get; set; }
        public string PushAppKey { get; set; }
        public string PushMasterKey { get; set; }
        public string QR_Secret { get; set; }
    }
    public partial class Company_Save_Object
    {
        public System.DateTime AddedDate { get; set; }
        public string ApprovalConfirmationEndpoint { get; set; }
        public string AsyncAuthenticateEndpoint { get; set; }
        public string ClientBaseUrl { get; set; }
        public string CompanyName { get; set; }
        public string EndpointUrl { get; set; }
        public Nullable<System.DateTime> ExpireDate { get; set; }
        public string IpAddress { get; set; }
        public bool IsActive { get; set; }
        public bool IsArchived { get; set; }
        public string IsUserExistingEndpoint { get; set; }
        public Nullable<System.DateTime> JoinDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public string UrbunAppKey { get; set; }
        public string UrbunMasterKey { get; set; }
        public string UserDetailsEndpoint { get; set; }
        public string QR_Secret { get; set; }
    }

}