﻿using System;
using System.Linq;
using Authnex_v2.Models.DB;


namespace Authnex_V2.Models.BL
{
    public class SessionBusniessLogics
    {
        public int getSessionID(int userID, String UUID)
        {
            using (var context = new Era_AuthnexDBEntities())
            {
                var result = context.Devices.SingleOrDefault(b => b.Uuid == UUID);
                if (result != null)
                {
                    int Device_ID = result.Device_ID;
                    var SessionResult = context.Sessions.SingleOrDefault(b => b.UserID == userID && b.DeviceID == Device_ID);
                    if (result != null)
                    {
                        return SessionResult.Session_ID;
                    }
                    else
                    {
                        return -1;
                    }
                }
                return -1;
            }
        }

        public bool validateSession(int userID, String UUID, String API_call)
        {
            int Device_ID;
            try
            {
                using (var context = new Era_AuthnexDBEntities())
                {
                    var result = context.Devices.SingleOrDefault(b => b.Uuid == UUID);
                    if (result != null)
                    {
                        Device_ID = result.Device_ID;
                        var SessionResult = context.Sessions.SingleOrDefault(b => b.UserID == userID && b.DeviceID == Device_ID);
                        if (result != null)
                        {
                            double SessionAge = Convert.ToDouble(SessionResult.SessionDate - DateTime.Now);
                            if (SessionAge > 1800)  // 30 Minius Exipre time
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        }
                        else
                        {
                            return false;
                        }
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                return false;
            }
        }
    }
}